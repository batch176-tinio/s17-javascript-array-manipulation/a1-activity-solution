// alert("Hello");

let student = [];

//---------------------------------
function addStudent(studentNametoAdd) {

	student.push(studentNametoAdd);
	console.log(`${studentNametoAdd} has been added to student list`);

}

//---------------------------------

function countStudents() {

	console.log(`There is a total of ${student.length} students enrolled.`);

}


//---------------------------------

function printStudents() {

	student.sort()
	student.forEach(function(s) {
	console.log(s);
})

}


//---------------------------------

function findStudent(studentNameToFind) {


	let filteredExactMatch = student.filter(function(s) {
		return (s.toLowerCase() === studentNameToFind.toLowerCase());
	})

	let filteredInexactMatch = student.filter(function(s) {
		return (s.toLowerCase().includes(studentNameToFind.toLowerCase()));
	})


	if (filteredExactMatch.length !== 0) {
		console.log(`${filteredExactMatch.join(` , `)} is an enrollee.`)
	} else if (filteredInexactMatch.length !== 0) {
		console.log(`${filteredInexactMatch.join(` , `)} are enrollees.`)
	} else {
		console.log(`No student found with the name ${studentNameToFind}.`)
	}

		
}

//---------------------------------

function addSection(section) {

	let studentMap = student.map(function(s){
		return s + ` - Section ${section}`

	})

	console.log(studentMap);
}

//---------------------------------

function removeStudent(studentNameToRemove) {

	let capitalFirstLetter = studentNameToRemove[0].toUpperCase() + studentNameToRemove.slice(1);

	let indexOfNameToRemove = student.indexOf(capitalFirstLetter);

	if (indexOfNameToRemove !== -1) {
		let removedStudent = student.splice((indexOfNameToRemove), 1);
		console.log(`${removedStudent} has been removed from the student list.`);
	} else {
		console.log(`${studentNameToRemove} is not on the student list.`)
	}
	
}



	














